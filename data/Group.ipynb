{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.integrate import quad, dblquad\n",
    "from sympy import diff\n",
    "from scipy.special import gamma"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cluster redshift, the angular size diameter, the kpc/arcmin to cm conversions, and the solar mass will be used often, so they are defined here. We also define the critical density at the cluster redshift."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "z = 0.546    # redshift of MACS J0717.5+3745\n",
    "kpc = 3.086e21    # 1 kpc in cm\n",
    "d_A = 1.3e6 * kpc    # cm\n",
    "arcmin_to_cm = 383.3 * kpc    # 1 arcmin = 383.3 kpc\n",
    "M_sun = 2e33    # solar mass in grams\n",
    "rho_crit = 1.7e-29    # g cm**-3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The temperature of the group is between 3.4 and 4.5 keV (1-sigma confidence ranges)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "T_low = 3.4    # keV\n",
    "T_high = 4.5    # keV"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The region from which the spectrum of the group was extracted is an ellipse with major axis 0.47 arcmin and minor axis 0.34 arcmin."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "a = 0.47 * arcmin_to_cm    # cm\n",
    "b = 0.34 * arcmin_to_cm    # cm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We consider two possible 3D geometries for the region chosen: a prolate spheroid with the major axis in the plane of the sky and aligned with the filament, and an oblate spheroid with the minor axis in the plane of the sky and perpendicular to the filament. The volumes of these two regions are:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The volumes of the spheroids considered are V1 = 0.018 Mpc**3 and V2 = 0.013 Mpc**3.\n"
     ]
    }
   ],
   "source": [
    "V1 = 4./3. * np.pi * a * b * a    # cm**3\n",
    "V2 = 4./3. * np.pi * a * b * b    # cm**3\n",
    "print(\"The volumes of the spheroids considered are V1 = %.3f Mpc**3 and V2 = %.3f Mpc**3.\" \n",
    "          % (V1/kpc**3/1e9, V2/kpc**3/1e9))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The normalization of the group's spectrum is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "norm = 1.8e-4    # cm**-5 arcmin**-2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assuming the density is constant in our region of interest, the Xspec normalization is defined as \n",
    "\n",
    "norm = n_e \\* n_H \\* cyl_V / ( 1e14 \\* area \\* pi \\* D_A\\*\\*2 \\* (1+z)\\*\\*2 )\n",
    "\n",
    "We assume the hydrogen number density relates to the electron number density as: n_e = 1.2 n_H. For simplicity, we'll also assume a volume equal to the average of the two volumes calculated above. Therefore, the hydrogen number density is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The mean mass density in the region of the group is 4.8e-27 g/cm**3 and it corresponds to an overdensity of ~285.\n"
     ]
    }
   ],
   "source": [
    "m_H = 1.7e-24    # grams\n",
    "area = np.pi * a * b / arcmin_to_cm**2\n",
    "V = (V1 + V2) / 2.\n",
    "n_H = np.sqrt( norm * 1e14 * area * 4 * np.pi * d_A**2 * (1+z)**2 / (1.2 * V) )\n",
    "rho_H = n_H * m_H\n",
    "delta = rho_H / rho_crit\n",
    "\n",
    "print(\"The mean mass density in the region of the group is %.2g g/cm**3 \\\n",
    "and it corresponds to an overdensity of ~%.0f.\" % (rho_H, delta))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The density corresponds to a gas mass of:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 46,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The gas mass of the group is ~1e+12 Msun.\n"
     ]
    }
   ],
   "source": [
    "M_gas = rho_H * V / M_sun\n",
    "print(\"The gas mass of the group is ~%.1g Msun.\" % M_gas)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on a beta model fitted to the group's surface brightness profile, the central surface brightness is 7.3e-15 erg/s/arcmin\\*\\*2. This yields a central density:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.00293727297009 0.00306824557367\n"
     ]
    }
   ],
   "source": [
    "S0 = 7.3e-15    # erg/s/arcmin**2\n",
    "S0_cgs = 1.6e-5 #S0 / arcmin_to_cm**2\n",
    "rc = 0.04 * arcmin_to_cm    # core radius in cm\n",
    "beta = 0.4\n",
    "\n",
    "cooling_function_low = 1/1.2 * (8.6e-3 * T_low**-1.7 + 5.8e-2 * T_low**0.5 + 6.4e-2) * 1e-22\n",
    "cooling_function_high = 1/1.2 * (8.6e-3 * T_high**-1.7 + 5.8e-2 * T_high**0.5 + 6.4e-2) * 1e-22\n",
    "n_high = np.sqrt(S0_cgs/ np.sqrt(np.pi) / rc / cooling_function_low / gamma(3.*beta - 0.5) * gamma(3.*beta))\n",
    "n_low = np.sqrt(S0_cgs/ np.sqrt(np.pi) / rc / cooling_function_high / gamma(3.*beta - 0.5) * gamma(3.*beta))\n",
    "print(n_low, n_high)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Under the assumption of hydrostatic equilibrium, the total mass of the group is defined by the equation is section 5.5.5 of Sarazin (1998)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def mass(r, T):"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
